/*=========================================================================
 *
 * This file was modified by Piotr M. Szczypinski in 2016.08 to:
 *  1. Enable computation of Frangi's and Erdt's vesselness
 *  2. Implement procedure for suppression of structures elongated in Z axis
 *  3. Optimize memory management
 *  4. Implement multithreading computation
 *
 * This code is based on ITK's Hessian3DToVesselnessMeasureImageFilter.
 *
 *=========================================================================*/


/*=========================================================================
 *
 *  Copyright Insight Software Consortium
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0.txt
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *=========================================================================*/
#ifndef itkScaffoldHessianToVesselnessFilter_h
#define itkScaffoldHessianToVesselnessFilter_h

#include "itkSymmetricSecondRankTensor.h"
#include "itkSymmetricEigenAnalysisImageFilter.h"
#include "itkDomainThreader.h"
#include "itkThreadedIndexedContainerPartitioner.h"


namespace itk
{
template< class TAssociate > class ScaffoldHessianToVesselnessFilterThreader :
public
itk::DomainThreader< itk::ThreadedIndexedContainerPartitioner, TAssociate >
{
public:
  // Standard ITK typedefs.
  typedef ScaffoldHessianToVesselnessFilterThreader            Self;
  typedef itk::DomainThreader<
    itk::ThreadedIndexedContainerPartitioner,
    TAssociate >                              Superclass;
  typedef itk::SmartPointer< Self >           Pointer;
  typedef itk::SmartPointer< const Self >     ConstPointer;

  // The domain is an index range.
  typedef typename Superclass::DomainType DomainType;

  // This creates the ::New() method for instantiating the class.
  itkNewMacro( Self );

protected:
  // We need a constructor for the itkNewMacro.
  ScaffoldHessianToVesselnessFilterThreader() {}

private:
  virtual void BeforeThreadedExecution()
    {
    const itk::ThreadIdType numberOfThreads = this->GetNumberOfThreadsUsed();
    }

  virtual void ThreadedExecution( const DomainType & subDomain,
                                  const itk::ThreadIdType threadId )
    {
    // Look only at the range of cells by the set of indices in the subDomain.
//    for( size_t ii = subDomain[0]; ii <= subDomain[1]; ++ii )
//      {
//      }
    }

  virtual void AfterThreadedExecution()
    {
    }
};





template< typename  TPixel >
class ScaffoldHessianToVesselnessFilter:public
        ImageToImageFilter< Image< SymmetricSecondRankTensor< double, 3 >, 3 >,
        Image< TPixel, 3 > >
{
public:
    typedef enum {
        Sato = 1,
        Frangi,
        Erdt,
//        Hannink,
        SatoXY,
        FrangiXY,
        ErdtXY
    } HessianToVesselnessMethod;

    /** Standard class typedefs. */
    typedef ScaffoldHessianToVesselnessFilter Self;
    typedef ScaffoldHessianToVesselnessFilterThreader< Self > ThreaderType;

    typedef ImageToImageFilter<
    Image< SymmetricSecondRankTensor< double, 3 >, 3 >,
    Image< TPixel, 3 > >                    Superclass;

    typedef SmartPointer< Self >       Pointer;
    typedef SmartPointer< const Self > ConstPointer;

    typedef typename Superclass::InputImageType  InputImageType;
    typedef typename Superclass::OutputImageType OutputImageType;
    typedef typename InputImageType::PixelType   InputPixelType;
    typedef TPixel                               OutputPixelType;

    /** Image dimension = 3. */
    itkStaticConstMacro(ImageDimension, unsigned int,
                        InputImageType ::ImageDimension);
    itkStaticConstMacro(InputPixelDimension, unsigned int,
                        InputPixelType::Dimension);

    typedef  FixedArray< double, itkGetStaticConstMacro(InputPixelDimension) >
    EigenValueArrayType;
    typedef  Image< EigenValueArrayType, itkGetStaticConstMacro(ImageDimension) >
    EigenValueImageType;
    typedef   SymmetricEigenAnalysisImageFilter<
    InputImageType, EigenValueImageType >     EigenAnalysisFilterType;

    typedef typename Superclass::OutputImageRegionType OutputImageRegionType;

    /** Run-time type information (and related methods).   */
    itkTypeMacro(ScaffoldHessianToVesselnessFilter, ImageToImageFilter);

    /** Method for creation through the object factory. */
    itkNewMacro(Self);

    /** Set/Get macros for parameters.
   * parameters ParamX have different meanings depending on the selected method */
    itkSetMacro(Param1, double);
    itkGetConstMacro(Param1, double);
    itkSetMacro(Param2, double);
    itkGetConstMacro(Param2, double);
    itkSetMacro(Param3, double);
    itkGetConstMacro(Param3, double);

    itkSetMacro(Method, HessianToVesselnessMethod);
    itkGetConstMacro(Method, HessianToVesselnessMethod);

    void SetMethodSato(double Alpha1 = 0.5, double Alpha2 = 2.0)
    {
        SetParam1(Alpha1);
        SetParam2(Alpha2);
        SetMethod(Sato);
    }
    void SetMethodFrangi(double c, double Alpha = 0.5, double Beta = 0.5)
    {
      SetParam1(c);
      SetParam2(Alpha);
      SetParam3(Beta);
      SetMethod(Frangi);
    }
    void SetMethodErdt(void)
    {
      SetMethod(Erdt);
    }
    void SetMethodSatoXY(double Alpha1 = 0.5, double Alpha2 = 2.0)
    {
        SetParam1(Alpha1);
        SetParam2(Alpha2);
        SetMethod(SatoXY);
    }
    void SetMethodFrangiXY(double c, double Alpha = 0.5, double Beta = 0.5)
    {
      SetParam1(c);
      SetParam2(Alpha);
      SetParam3(Beta);
      SetMethod(FrangiXY);
    }
    void SetMethodErdtXY(void)
    {
      SetMethod(ErdtXY);
    }


#ifdef ITK_USE_CONCEPT_CHECKING
    // Begin concept checking
    itkConceptMacro( DoubleConvertibleToOutputCheck,
                     ( Concept::Convertible< double, OutputPixelType > ) );
    // End concept checking
#endif

protected:
    ScaffoldHessianToVesselnessFilter();
    ~ScaffoldHessianToVesselnessFilter() {}
    void PrintSelf(std::ostream & os, Indent indent) const ITK_OVERRIDE;

    /** Generate Data */
    //void GenerateData(void) ITK_OVERRIDE;
    void BeforeThreadedGenerateData(void) ITK_OVERRIDE;
    void ThreadedGenerateData(const OutputImageRegionType &, ThreadIdType) ITK_OVERRIDE;

private:
    friend class ScaffoldHessianToVesselnessFilterThreader< Self >;
    typename ThreaderType::Pointer m_Threader;

    ScaffoldHessianToVesselnessFilter(const Self &) ITK_DELETE_FUNCTION;
    void operator=(const Self &) ITK_DELETE_FUNCTION;

    typename EigenAnalysisFilterType::Pointer m_SymmetricEigenValueFilter;

    HessianToVesselnessMethod m_Method;
    double m_Param1;
    double m_Param2;
    double m_Param3;
};
} // end namespace itk

#ifndef ITK_MANUAL_INSTANTIATION
#include "itkScaffoldHessianToVesselnessFilter.hxx"
#endif

#endif
