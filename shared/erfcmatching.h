#ifndef ERFCMATCHING_H
#define ERFCMATCHING_H
#include <vector>

std::vector<double> erfcMatching(std::vector<std::vector<double> > &input);
//double erfcMatching(std::vector<double> input);

#endif // ERFCMATCHING_H
