#ifndef THRSMATCHING_H
#define THRSMATCHING_H

#include <vector>
extern double image_threshold;

std::vector<double> thrsMatching(std::vector<std::vector<double> > &input);

#endif // THRSMATCHING_H
