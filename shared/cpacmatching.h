#ifndef CPACMATCHING_H
#define CPACMATCHING_H

#include <vector>

extern double snake_initial_radius;
extern double snake_image;
extern double snake_image_threshold;
extern double snake_alpha;
extern double snake_beta;
extern double snake_gamma;
extern unsigned int snake_iterations;

std::vector<double> cpacMatching(std::vector<std::vector<double> > &input);

#endif // CPACMATCHING_H
