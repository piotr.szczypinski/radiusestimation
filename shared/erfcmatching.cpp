/*
 * erfcMatching.cpp
 * Copyright 2020 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


// Ref: https://github.com/davisking/dlib/blob/master/examples/least_squares_ex.cpp

#include <math.h>
#include <dlib/matrix.h>
#include <dlib/optimization.h>

#include "erfcmatching.h"

typedef dlib::matrix<double, 1, 1> InputData;
typedef dlib::matrix<double, 3, 1> Parameters;

double model (
    const InputData& data,
    const Parameters& params
)
{
    const double v0 = params(0);
    const double dv = params(1);
    const double r = params(2);
    const double d = data(0);

    return v0+dv*erfc(d-r);
//    return v0+dv*(1.0-erf(d-r));
}

double residual (
    const std::pair<InputData, double>& data,
    const Parameters& params
)
{
    return model(data.first, params) - data.second;
}

double erfcMatchingSingle(std::vector<double>& input)
{
    try
    {
        std::vector<std::pair<InputData, double> > samples;
        InputData m;
        for(double f : input)
        {
            m(0) = samples.size();
            samples.push_back(std::make_pair(m, f));
        }
        Parameters params;
        params(0) = 1.0; //v0
        params(1) = 0.5; //dv
        params(2) = 1.0; //r
        dlib::solve_least_squares_lm(dlib::objective_delta_stop_strategy(1e-7)/*.be_verbose()*/,
                                     residual,
                                     dlib::derivative(residual),
                                     samples,
                                     params);
        return params(2);
    }
    catch (...)
    {
        return -1.0;
    }
}

std::vector<double> erfcMatching(std::vector<std::vector<double> >& input)
{
    unsigned int directions = input.size();
    std::vector<double> rad;
    rad.resize(directions);
    for(unsigned int i = 0; i < directions; i++)
    {
        rad[i] = erfcMatchingSingle(input[i]);
    }
    return rad;
}
