/*
 * vesseltree.cpp
 * This code is a modified excerpt from VesselKnife project:
 * https://gitlab.com/vesselknife/vesselknife
 *
 * Copyright 2016-2020 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "vesseltree.h"
#include <fstream>

bool saveTree(const char *fileName, Tree& tree)
{
    std::ofstream file;
    file.open(fileName);
    if (!file.is_open()) return false;
    if (!file.good()) return false;

    file << "@TreeSkeleton2014_Internal"<< std::endl;
    file << "@NumberOfAllNodes " << tree.nodes.size() << std::endl;
    for(std::vector<Node>::iterator n = tree.nodes.begin(); n != tree.nodes.end(); ++n)
    {
        file << "\t" << n->x << " " << n->y << " " << n->z << " " << n->connections << " "<< n->radius << std::endl;
    }
    file << "@NumberOfBranches " << tree.branches.size() << std::endl;
    for(std::vector<Branch>::iterator b = tree.branches.begin(); b != tree.branches.end(); ++b)
    {
        file << "\t" << b->node_indices.size();
        for(std::vector<unsigned int>::iterator n = b->node_indices.begin(); n != b->node_indices.end(); ++n)
        {
            file << " " << *n;
        }
        file << std::endl;
    }
    file.close();
    return true;
}

Tree loadTree(const char *fileName)
{
    Tree tree;
    std::ifstream file;
    file.open(fileName);
    if (!file.is_open()) return tree;
    if (!file.good()) return tree;

    std::string inputstring;
    file >> inputstring;

    if(inputstring == "@TreeSkeleton2014_Internal")
    {
        int NumberOfAllNodes;
        file>>inputstring;
        if(inputstring != "@NumberOfAllNodes")
            return tree;
        file>>NumberOfAllNodes;
        if(NumberOfAllNodes <= 0)
            return tree;
        for(int n = 0; n < NumberOfAllNodes; n++)
        {
            Node newnode;
            file >> newnode.x >> newnode.y >> newnode.z >> newnode.connections >> newnode.radius;
            tree.nodes.push_back(newnode);
        }

        int NumberOfBranches;
        file>>inputstring; if(inputstring != "@NumberOfBranches")
            return tree;
        file>>NumberOfBranches;
        if(NumberOfBranches <= 0)
            return tree;
        for(int b = 0; b < NumberOfBranches; b++)
        {
            Branch newbranch;
            int NumberOfNodes;
            file>>NumberOfNodes;
            if(NumberOfNodes <= 0)
                return tree;
            for(int n = 0; n < NumberOfNodes; n++)
            {
                int newindex;
                file>>newindex;
                if(newindex < 0 || newindex >= NumberOfAllNodes)
                    return tree;
                newbranch.node_indices.push_back(newindex);
            }
            tree.branches.push_back(newbranch);
        }
    }
    else
        return tree;

    file.close();
    return tree;
}
