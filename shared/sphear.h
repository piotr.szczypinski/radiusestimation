/*
 * sphear.h
 * Copyright 2006-2020 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef SPHEAR_H
#define SPHEAR_H

const double wsp12[12][3] =
{
  {0.0000000000, 0.0000000000, 1.0000000000},
  {0.0000000000, 0.8944271910, 0.4472135950},
  {0.8506508080, 0.2763932020, 0.4472135950},
  {0.5257311120, -0.7236067970, 0.4472135950},
  {-0.5257311120, -0.7236067970, 0.4472135950},
  {-0.8506508080, 0.2763932020, 0.4472135950},
  {-0.8506508080, -0.2763932020, -0.4472135950},
  {-0.5257311120, 0.7236067970, -0.4472135950},
  {0.5257311120, 0.7236067970, -0.4472135950},
  {0.8506508080, -0.2763932020, -0.4472135950},
  {0.0000000000, -0.8944271910, -0.4472135950},
  {0.0000000000, 0.0000000000, -1.0000000000},
};

const double pol12[12][6] =
{
  {1, 5, 4, 3, 2, -1},
  {0, 2, 8, 7, 5, -1},
  {0, 3, 9, 8, 1, -1},
  {0, 4, 10, 9, 2, -1},
  {0, 5, 6, 10, 3, -1},
  {0, 1, 7, 6, 4, -1},
  {4, 5, 7, 11, 10, -1},
  {1, 8, 11, 6, 5, -1},
  {1, 2, 9, 11, 7, -1},
  {2, 3, 10, 11, 8, -1},
  {3, 4, 6, 11, 9, -1},
  {6, 7, 8, 9, 10, -1},
};

struct Vertex6N
{
  double x;
  double y;
  double z;
  int neighbors[6];
};

class Sphear
{
public:
    Sphear(int divisions = 0);
    ~Sphear();

    void triangles(unsigned int* triangles);
    void edges(unsigned int *edges);
    void vertex(unsigned int index, double V[3]);
    unsigned int trianglesNumber();
    unsigned int edgesNumber();
    unsigned int verticesNumber();

private:
    int verticesNumb;
    Vertex6N *vertices;
    void Create6neighbor(int divisions);
};

#endif // SPHEAR_H
