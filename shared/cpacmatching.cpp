#include "cpacmatching.h"
#include <dlib/matrix.h>

double snake_initial_radius = 1.0;
double snake_image = 0.1;
double snake_image_threshold = 128;
double snake_alpha = 0.8;
double snake_beta = 0.1;
double snake_gamma = 1;
unsigned int snake_iterations = 100;

const double pi2 = 1.570796326794897;

void internalForce(dlib::matrix<double>& matrix, std::vector<double>& inr,  std::vector<double>* outr)
{
    unsigned int jmax = matrix.nc();
    unsigned int imax = matrix.nr();

    for(unsigned int i = 0; i < imax; i++)
    {
        (*outr)[i] = 0;
        for(unsigned int j = 0; j < jmax; j++)
        {
            (*outr)[i] += inr[j] * matrix(i, j);
        }
    }
}

double externalForce(std::vector<double>& image_line, double r)
{
    if(r < 0.0)
        return 0;
    unsigned int ir = (int)r;
    if(ir+1 >= image_line.size())
        return 0;
    double drl = r-ir;
    double value;
    value  = drl*image_line[ir+1] + (1.0 - drl)*image_line[ir];
    return snake_image*(value-snake_image_threshold);
}

std::vector<double> cpacMatching(std::vector<std::vector<double>>& input)
{
    unsigned int snake_directions = input.size();

    dlib::matrix<double> pentadiagonal = dlib::identity_matrix<double>(snake_directions);
    for(unsigned int i = 0; i < snake_directions; i++)
    {
        pentadiagonal(i, (i+snake_directions-2)%snake_directions) = pentadiagonal(i,(i+2)%snake_directions) = snake_beta;
        pentadiagonal(i,(i+snake_directions-1)%snake_directions) = pentadiagonal(i,(i+1)%snake_directions) = -4*snake_beta -snake_alpha;
        pentadiagonal(i,i) = 6*snake_beta +2*snake_alpha +snake_gamma;
    }
    dlib::matrix<double> inversed_pentadiagonal = dlib::inv(pentadiagonal);
    std::vector<double> rad;
    std::vector<double> rad_temp;

    rad.resize(snake_directions, snake_initial_radius);
    rad_temp.resize(snake_directions, 0.0);

    for(unsigned int i = 0; i < snake_iterations; i++)
    {
        double weight = cos(pi2* (double)i/snake_iterations);
        for(unsigned int s = 0; s < snake_directions; s++)
        {
            double ef = externalForce(input[s], rad[s]);
            if(ef == ef)
                rad_temp[s] = rad[s] + (externalForce(input[s], rad[s]) * weight);
            else
                rad_temp[s] = rad[s];
        }
        internalForce(inversed_pentadiagonal, rad_temp, &rad);
        for(unsigned int s = 0; s < snake_directions; s++)
        {
            rad[s] = rad[s]*weight + rad_temp[s]*(1.0-weight);
            if(rad[s] < 0.5)
                rad[s] = 0.5;
            if(rad[s] > input[s].size()-2)
                rad[s] = input[s].size()-2;
        }
    }
    return rad;
}


