#include "thrsmatching.h"

double image_threshold = 128;

double thrsMatchingSingle(std::vector<double>& input)
{
    unsigned int imax = input.size();
    if(input[0] < image_threshold)
    {
        for(unsigned int i = 1; i < imax; i++)
        {
            if(input[i] > image_threshold)
                return (double) i -0.5;
            if(input[i] == image_threshold)
                return i;
        }
        return imax;
    }
    else if(input[0] > image_threshold)
    {
        for(unsigned int i = 1; i < imax; i++)
        {
            if(input[i] < image_threshold)
                return (double) i -0.5;
            if(input[i] == image_threshold)
                return i;
        }
        return imax;
    }
    else
        return 0;
}

std::vector<double> thrsMatching(std::vector<std::vector<double> >& input)
{
    unsigned int directions = input.size();
    std::vector<double> rad;
    rad.resize(directions);
    for(unsigned int i = 0; i < directions; i++)
    {
        rad[i] = thrsMatchingSingle(input[i]);
    }
    return rad;
}
