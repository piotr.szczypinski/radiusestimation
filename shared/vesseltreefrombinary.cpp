/*
 * vesseltreefrombinary.cpp
 * This code is a modified excerpt from VesselKnife project:
 * https://gitlab.com/vesselknife/vesselknife
 *
 * Copyright 2016-2020 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "vesseltree.h"
#include "vesseltreefrombinary.h"
#include <fstream>

const int neighborhoodScanSequence[27][3] =
{
    {0,0,0}, //0 center
    {0,0,1},{0,0,-1},{0,1,0},{0,-1,0},{1,0,0},{-1,0,0}, //1-6 facet neighbor
    {0,1,1},{0,1,-1},{0,-1,1},{0,-1,-1},{1,1,0},{1,-1,0},{-1,1,0},{-1,-1,0},{1,0,1},{1,0,-1},{-1,0,1},{-1,0,-1}, //7-18 edge neighbor
    {1,1,1},{1,1,-1},{1,-1,1},{1,-1,-1},{-1,1,1},{-1,1,-1},{-1,-1,1},{-1,-1,-1} //19-26 vertex neighbor
};
struct StackElement
{
    unsigned int index;
    unsigned int n;
};

void findAllNodes(ImageType::Pointer bufferImage, std::vector<ImageType::IndexType>* seedlist)
{
    itk::ImageRegionIterator< ImageType > iterator;
    iterator = itk::ImageRegionIterator< ImageType >(bufferImage, bufferImage->GetRequestedRegion());

    iterator.GoToBegin();
    while (!iterator.IsAtEnd())
    {
        if (iterator.Get() > 0)
        {
            seedlist->push_back(iterator.GetIndex());
        }
        ++iterator;
    }
}


void skeletonToTreeIntSpace(ImageType::Pointer bufferImage,
                            Tree* treeToCreate,
                            ImageType::IndexType seedToBegin)
{
    unsigned int i;
    // Stack of bifurcation points to go back and check for connected voxels
    typedef itk::ConstantBoundaryCondition< ImageType > BoundaryConditionType;
    typedef itk::NeighborhoodIterator< ImageType, BoundaryConditionType> NeighborhoodIteratorType;
    NeighborhoodIteratorType::RadiusType radius;
    ImageType::IndexType region_index;
    ImageType::SizeType region_size;
    ImageType::RegionType region;
    for (i = 0; i < ImageType::ImageDimension; ++i)
    {
        radius[i] = 1;
        region_index[i] = 0;
    }
    region_size = bufferImage->GetRequestedRegion().GetSize();
    region.SetSize(region_size);
    region.SetIndex(region_index);

    itk::NeighborhoodIterator<ImageType> image_iterator(radius, bufferImage, region);
    int sc = image_iterator.Size() / 2;
    int sy = image_iterator.GetStride(1);
    int sz = image_iterator.GetStride(2);

    StackElement stack_element;
    Node new_node;
    std::vector<StackElement> bifur_node_index_stack;
    Branch new_branch;

    // Initialize stack with the new node (seed) index
    unsigned int new_node_index = treeToCreate->nodes.size();
    stack_element.n = 1;
    stack_element.index = new_node_index;
    bifur_node_index_stack.push_back(stack_element);
    // Create new branch to be added to tree when complete
    new_branch.node_indices.push_back(new_node_index);
    // Add a seed to the tree nodes vector
    new_node.x = seedToBegin[0];
    new_node.y = seedToBegin[1];
    new_node.z = seedToBegin[2];
    new_node.connections = 0;
    new_node.radius = 0;
    treeToCreate->nodes.push_back(new_node);
    bufferImage->SetPixel(seedToBegin, 0);

    while(bifur_node_index_stack.size() > 0)
    {
        if(bifur_node_index_stack.back().n >= 27)
        {
            //This is the last node in branch with no more connections to verify
            bifur_node_index_stack.pop_back();
            if(new_branch.node_indices.size() > 0)
            {
                if(new_branch.node_indices.size() > 1 || treeToCreate->nodes[new_branch.node_indices.back()].connections < 1)
                {
                    treeToCreate->branches.push_back(new_branch);
                    new_branch.node_indices.clear();
                }
                else new_branch.node_indices.clear();
            }
        }
        else
        {
            new_node_index = bifur_node_index_stack.back().index;
            new_node = treeToCreate->nodes[new_node_index];
            region_index[0] = new_node.x;
            region_index[1] = new_node.y;
            region_index[2] = new_node.z;
            region.SetIndex(region_index);

            if(new_branch.node_indices.size() <= 0)
            {
                new_branch.node_indices.push_back(new_node_index);
            }

            itk::NeighborhoodIterator<ImageType> iterator(radius, bufferImage, region);
            for(; bifur_node_index_stack.back().n < 27; bifur_node_index_stack.back().n++)
            {
                const int* nss = neighborhoodScanSequence[bifur_node_index_stack.back().n];
                int offset = sc+nss[0]+nss[1]*sy+nss[2]*sz;

                if(iterator.GetPixel(offset) > 0)
                {
                    // Set voxel to zero
                    bufferImage->SetPixel(iterator.GetIndex(offset), 0);
                    // Next node to add to branch was found
                    ImageType::IndexType image_index = iterator.GetIndex(offset);
                    new_node.x = image_index[0];
                    new_node.y = image_index[1];
                    new_node.z = image_index[2];
                    new_node.radius = 0;
                    new_node.connections = 1;
                    treeToCreate->nodes[new_branch.node_indices.back()].connections++;
                    new_node_index = treeToCreate->nodes.size();
                    treeToCreate->nodes.push_back(new_node);
                    stack_element.n = 1;
                    stack_element.index = new_node_index;
                    bifur_node_index_stack.push_back(stack_element);
                    new_branch.node_indices.push_back(new_node_index);
                    break;
                }
            }
        }
    }
}

Tree treeFromSkeleton(ImageType::Pointer imageBuffer, ImageType::IndexType* seedLocation)
{
    unsigned int iimax, ii;
    std::vector<ImageType::IndexType> seedlist;
    findAllNodes(imageBuffer, &seedlist);
    // We force the tree will grow from only one seed.
    // Result must be single-connected.
    // Unconnected branches will be disregarded.
    if(seedLocation != NULL)
    {
        iimax = seedlist.size();
        double distance = std::numeric_limits<double>::max();
        unsigned int indseed = 0;
        for(ii = 0; ii < iimax; ii++)
        {
            double x = (*seedLocation)[0] - seedlist[ii][0];
            double y = (*seedLocation)[1] - seedlist[ii][1];
            double z = (*seedLocation)[2] - seedlist[ii][2];
            double d = x*x + y*y + z*z;
            if(d < distance)
            {
                distance = d;
                indseed = ii;
            }
        }
        ImageType::IndexType seed = seedlist[indseed];
        seedlist.clear();
        seedlist.push_back(seed);
    }
    // Building trees.
    // For loop to create disconnected trees.
    Tree tree_struct;
    iimax = seedlist.size();
    for(ii = 0; ii < iimax; ++ii)
    {
        if(imageBuffer->GetPixel(seedlist[ii]) > 0)
        {
            skeletonToTreeIntSpace(imageBuffer, &tree_struct, seedlist[ii]);
        }
    }
    // Computation of tree nodes' coordinates in image (metric) space
    iimax = tree_struct.nodes.size();
    ImageType::SpacingType spacing = imageBuffer->GetSpacing();
    for(ii = 0; ii<iimax; ii++)
    {
        tree_struct.nodes[ii].x *= spacing[0];
        tree_struct.nodes[ii].y *= spacing[1];
        tree_struct.nodes[ii].z *= spacing[2];
        tree_struct.nodes[ii].radius = 0;
    }
    return tree_struct;
}

ImageType::Pointer skeletonFromBinaryImage(ImageType::Pointer input_image)
{
    typedef itk::BinaryThinningImageFilter3D < ImageType, ImageType > F;
    typedef F::Pointer P;
    P filter = F::New();
    filter->SetInput(input_image);
    filter->Update();
    return filter->GetOutput();
}

Tree treeFromBinaryImage(ImageType::Pointer image, ImageType::IndexType* seed)
{
    ImageType::Pointer binaryimage = skeletonFromBinaryImage(image);
    Tree tree = treeFromSkeleton(binaryimage, seed);
    return tree;
}
