/*
 * It estimates radius out of the partial radii found in directions
 * in 3D space. Compares two methods for finding borders of tubes:
 *   - grey-level thresholding
 *   - erfc function fitting
 *
 * Copyright 2020 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
 * Requires dlib and ITK:
 *      http://dlib.net
 *      https://github.com/davisking/dlib
 *      https://itk.org/
 */


#include <math.h>
#include <stdio.h>
#include <vector>

#include "../shared/erfcmatching.h"
#include "../shared/cpacmatching.h"
#include "../shared/thrsmatching.h"
#include "../shared/sphear.h"

#include "ITKIOFactoryRegistration/itkImageIOFactoryRegisterManager.h"
#include "ITKIOFactoryRegistration/itkTransformIOFactoryRegisterManager.h"
#include <itkImageIOFactory.h>
#include <itkImageFileReader.h>
#include <itkContinuousIndex.h>
#include <itkLinearInterpolateImageFunction.h>
#include <itkBinaryThresholdImageFilter.h>
#include <itkBinaryFillholeImageFilter.h>

#include "../shared/vesseltreefrombinary.h"
#include <dlib/matrix.h>
typedef dlib::matrix<double, 3, 3> Covarm;

//https://gist.github.com/dennisfrancis/444dd3e918f61f898f9d1551eeb8cde0

const double pi = 3.141592653589793;
const double p2 = 1.414213562373095;

const unsigned int sphear_divisions = 4;
//const unsigned int num_of_dirs = 15;
const unsigned int num_of_samples = 100;
const double dist_of_samples = 0.2;
const unsigned char threshold_value = 159;

ImageType::Pointer thresholdFilter(ImageType::Pointer input_image, PixelType min, PixelType max)
{
    typedef itk::BinaryThresholdImageFilter< ImageType, ImageType > F;
    typedef F::Pointer P;
    P filter = F::New();
    filter->SetInput(input_image);
    filter->SetLowerThreshold(min);
    filter->SetUpperThreshold(max);
    filter->SetInsideValue(255);
    filter->SetOutsideValue(0);
    filter->Update();
    return filter->GetOutput();
}

ImageType::Pointer fillInBlackHoles(ImageType::Pointer input_image)
{
    typedef typename itk::BinaryFillholeImageFilter < ImageType > F;
    typedef typename F::Pointer P;
    P filter = F::New();
    filter->SetInput(input_image);
    filter->SetForegroundValue(255);
    filter->Update();
    return filter->GetOutput();
}

struct Vector3
{
    double x;
    double y;
    double z;
};

std::vector <Vector3> findVectorsFromPartialEstimates(std::vector<double>& estimates, Sphear& sphear)
{
    std::vector <Vector3> vectors;
    unsigned int num_of_dirs = sphear.verticesNumber();
    vectors.resize(num_of_dirs);
    for(unsigned int d = 0; d < num_of_dirs; d++)
    {
        double vector[3];
        sphear.vertex(d, vector);

        vectors[d].x = vector[0]*estimates[d];
        vectors[d].y = vector[1]*estimates[d];
        vectors[d].z = vector[2]*estimates[d];
    }
    return vectors;
}

void estimateRadiusByPCA(std::vector <Vector3>& vectors, double radius[2])
{
    double varx = 0.0;
    double vary = 0.0;
    double varz = 0.0;
    double varxy = 0.0;
    double varxz = 0.0;
    double varyz = 0.0;

    unsigned int imax = vectors.size();
    for(unsigned int i = 0; i < imax; i++)
    {
        varx += vectors[i].x*vectors[i].x;
        vary += vectors[i].y*vectors[i].y;
        varz += vectors[i].z*vectors[i].z;
        varxy += vectors[i].x*vectors[i].y;
        varxz += vectors[i].x*vectors[i].z;
        varyz += vectors[i].y*vectors[i].z;
    }
    Covarm covarm;
    covarm(0, 0) = varx/imax;
    covarm(1, 1) = vary/imax;
    covarm(2, 2) = varz/imax;
    covarm(0, 1) = covarm(1, 0) = varxy/imax;
    covarm(0, 2) = covarm(2, 0) = varxz/imax;
    covarm(2, 1) = covarm(1, 2) = varyz/imax;

    dlib::eigenvalue_decomposition<Covarm> eigdec(covarm);
    auto eigvals = eigdec.get_real_eigenvalues();

    radius[0] = sqrt(2.0*fabs(eigvals(0)));
    radius[1] = sqrt(fabs(eigvals(0)) + fabs(eigvals(1)));
}

int main(int argc, char *argv[])
{
    if(argc < 2)
        return 1;

    double ground_truth_radius = 0;
    if(argc > 2)
        ground_truth_radius = atof(argv[2]);

    typedef itk::ImageFileReader<ImageType> ReaderType;
    ReaderType::Pointer reader = ReaderType::New();
    reader->SetFileName(argv[1]);
    try
    {
        reader->Update();
    }
    catch (itk::ExceptionObject &ex)
    {
        std::cout << ex << std::endl;
        return false;
    }
    ImageType::Pointer image = reader->GetOutput();
    Tree tree;
    if(argc > 3)
        tree = loadTree(argv[3]);

    if(tree.branches.size() == 0 || tree.nodes.size() == 0)
    {
        ImageType::Pointer thresholded_image = thresholdFilter(image, threshold_value, 255);
        ImageType::Pointer binary_image = fillInBlackHoles(thresholded_image);
        tree = treeFromBinaryImage(binary_image);
        if(argc > 3)
            saveTree(argv[3], tree);
    }

    itk::ContinuousIndex<double, 3> pixel;
    itk::LinearInterpolateImageFunction<ImageType, double>::Pointer interpolator =
      itk::LinearInterpolateImageFunction<ImageType, double>::New();
    interpolator->SetInputImage(image);
    ImageType::SizeType imagesize = image->GetLargestPossibleRegion().GetSize();
    Sphear sphear(sphear_divisions);
    double erfc1_average = 0.0;
    double erfc1_squared = 0.0;
    double erfc2_average = 0.0;
    double erfc2_squared = 0.0;
    double thrs1_average = 0.0;
    double thrs1_squared = 0.0;
    double thrs2_average = 0.0;
    double thrs2_squared = 0.0;

    unsigned int number_of_nodes = 0;
    unsigned int number_of_branches = 0;
    unsigned int bmax = tree.branches.size();
    for(unsigned int b = 0; b < bmax; b++)
    {
        unsigned int nmax = tree.branches[b].node_indices.size();
        if(nmax < 3)
            continue;
        bool countthis = false;
        for(unsigned int n = 1; n < nmax-1; n++)
        {
            Node* nn = &(tree.nodes[tree.branches[b].node_indices[n]]);
            double x = nn->x - (double)imagesize[0]/2.0;
            double y = nn->y - (double)imagesize[1]/2.0;
            double z = nn->z - (double)imagesize[2]/2.0;
            if(x*x+y*y+z*z > 25*25)
                continue;

            std::vector<std::vector<double> > data;
            unsigned int num_of_dirs = sphear.verticesNumber();
            for(unsigned int d = 0; d < num_of_dirs; d++)
            {
                std::vector<double> line;
                line.resize(num_of_samples);
                double vector[3];
                sphear.vertex(d, vector);
                for(unsigned int i = 0; i < num_of_samples; i++)
                {
                    pixel[0] = vector[0]*dist_of_samples*i + nn->x;
                    pixel[1] = vector[1]*dist_of_samples*i + nn->y;
                    pixel[2] = vector[2]*dist_of_samples*i + nn->z;
                    if(pixel[0] < 0) pixel[0] = 0;
                    if(pixel[0] > imagesize[0]-1) pixel[0] = imagesize[0]-1;
                    if(pixel[1] < 0) pixel[1] = 0;
                    if(pixel[1] > imagesize[1]-1) pixel[1] = imagesize[1]-1;
                    if(pixel[2] < 0) pixel[2] = 0;
                    if(pixel[2] > imagesize[2]-1) pixel[2] = imagesize[2]-1;
                    line[i] = interpolator->EvaluateAtContinuousIndex(pixel);
                }
                data.push_back(line);
            }
            countthis = true;

            double r;
            image_threshold = threshold_value;
            std::vector<double> pthrs = thrsMatching(data);
            std::vector<Vector3> vthrs = findVectorsFromPartialEstimates(pthrs, sphear);
            double rthrs[2];
            estimateRadiusByPCA(vthrs, rthrs);
            r =  dist_of_samples*rthrs[0];
            thrs1_average += r;
            thrs1_squared += r*r;
            r =  dist_of_samples*rthrs[1];
            thrs2_average += r;
            thrs2_squared += r*r;

            std::vector<double> perfc = erfcMatching(data);
            std::vector<Vector3> verfc = findVectorsFromPartialEstimates(perfc, sphear);
            double rerfc[2];
            estimateRadiusByPCA(verfc, rerfc);
            r =  dist_of_samples*rerfc[0];
            erfc1_average += r;
            erfc1_squared += r*r;
            r =  dist_of_samples*rerfc[1];
            erfc2_average += r;
            erfc2_squared += r*r;
            number_of_nodes++;
        }
        if(countthis)
            number_of_branches++;
    }

    thrs1_average /= number_of_nodes;
    thrs1_squared /= number_of_nodes;
    thrs1_squared = sqrt(thrs1_squared - thrs1_average*thrs1_average);

    thrs2_average /= number_of_nodes;
    thrs2_squared /= number_of_nodes;
    thrs2_squared = sqrt(thrs2_squared - thrs2_average*thrs2_average);

    erfc1_average /= number_of_nodes;
    erfc1_squared /= number_of_nodes;
    erfc1_squared = sqrt(erfc1_squared - erfc1_average*erfc1_average);

    erfc2_average /= number_of_nodes;
    erfc2_squared /= number_of_nodes;
    erfc2_squared = sqrt(erfc2_squared - erfc2_average*erfc2_average);

    printf("%i, %i, %f, %f, %f %f, %f, %f, %f, %f, %f\n",
           number_of_branches, number_of_nodes,
           ground_truth_radius,
           thrs1_average, thrs1_squared, thrs2_average, thrs2_squared,
           erfc1_average, erfc1_squared, erfc2_average, erfc2_squared);
    return 0;
}
