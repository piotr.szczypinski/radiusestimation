TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

DESTDIR         = ../build
TARGET = radius3D

SOURCES += \
    radius3D.cpp \
    ../shared/erfcmatching.cpp \
    ../shared/thrsmatching.cpp \
    ../shared/vesseltree.cpp \
    ../shared/sphear.cpp \
    ../shared/vesseltreefrombinary.cpp

INCLUDEPATH += /home/piotr/Program/ThirdParty/usr/include/
LIBS += -lblas
LIBS += -llapack
LIBS += /home/piotr/Program/ThirdParty/usr/lib/libdlib.a

HEADERS += \
    ../shared/erfcmatching.h \
    ../shared/thrsmatching.h \
    ../shared/vesseltreefrombinary.h

INCLUDEPATH += ../../vesselknife/
include(../../vesselknife/Pri/itk.pri)
