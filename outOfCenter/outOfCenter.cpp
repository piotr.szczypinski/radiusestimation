/*
 * Illustration of discussion on methods for radius estimation.
 * The radius is estimated around the centerline, however the centerline
 * may be localized with out-of-center error /g/.
 *
 * Copyright 2020 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <math.h>
#include <stdio.h>

const double pi = 3.14159265358979323846264;
const double sqrt2 = 1.41421356237309504880169;
const double real_r = 1.0;
const unsigned int kmax = 100; //number of g values in range from 0 to real_r
const unsigned int imax = 10000; //number of integration samples/points

double delta_denom(double a, double g, double r)
{
    double gc = g*cos(a);
    return sqrt(gc*gc + r*r - g*g);
}
double x1(double a, double g, double sqrtCosGRG)
{
    return cos(a)*(cos(a)*g + sqrtCosGRG);
}
double x2(double a, double g, double sqrtCosGRG)
{
    return cos(a)*(cos(a)*g - sqrtCosGRG);
}
double y1(double a, double g, double sqrtCosGRG)
{
    return sin(a)*(cos(a)*g + sqrtCosGRG);
}
double y2(double a, double g, double sqrtCosGRG)
{
    return sin(a)*(cos(a)*g - sqrtCosGRG);
}
void integrate(double* px, double* py, double* pr, double* ps, double g, double r, unsigned int imax)
{
    double Ex2 = 0.0;
    double Ey2 = 0.0;
    double Ear = 0.0;
    double Esr = 0.0;

    for(unsigned int i = 0; i < imax; i++)
    {
        double a = pi*i/imax;
        double _delta_denom = delta_denom(a, g, r);
        double _x1 = x1(a, g, _delta_denom);
        double _x2 = x2(a, g, _delta_denom);
        double _y1 = y1(a, g, _delta_denom);
        double _y2 = y2(a, g, _delta_denom);

        Ex2 += _x1*_x1;
        Ex2 += _x2*_x2;
        Ey2 += _y1*_y1;
        Ey2 += _y2*_y2;
        Ear += sqrt(_x1*_x1+_y1*_y1);
        Ear += sqrt(_x2*_x2+_y2*_y2);
        Esr += (_x1*_x1+_x2*_x2+_y1*_y1+_y2*_y2);
    }
    *px = Ex2 / imax / 2.0;
    *py = Ey2 / imax / 2.0;
    *pr = Ear / imax / 2.0;
    *ps = Esr / imax / 2.0;
}

int main(void)
{
    double lx, ly, ar, as;
    printf("g\tr_x\tr_y\tr_xy\tr_aver\tr_area\n");
    for(unsigned int k = 0; k <= kmax; k++)
    {
        double g = (double)k*real_r/kmax;
        integrate(&lx, &ly, &ar, &as, g, real_r, imax);
        double llr = sqrt(lx+ly);
        lx = sqrt2*sqrt(lx);
        ly = sqrt2*sqrt(ly);
        printf("%f\t%f\t%f\t%f\t%f\t%f\n", (float)g, (float)lx, (float)ly, (float)llr, (float)ar, (float)as);
        fflush(stdout);
    }
    return 0;
}
