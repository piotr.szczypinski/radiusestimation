TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

DESTDIR         = ../build
TARGET = outOfCenter

SOURCES += \
    outOfCenter.cpp
