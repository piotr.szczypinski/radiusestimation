/*
 * It estimates radius out of the partial radii found in several directions. 
 * Compares three methods for finding borders of 2D circles:
 *   - center-point active contour region-based
 *   - center-point active contour gradient-based
 *   - grey-level thresholding
 *   - erfc function fitting
 *
 * Copyright 2020 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
 * Requires dlib and ITK:
 *      http://dlib.net
 *      https://github.com/davisking/dlib
 *      https://itk.org/
 */


#include <math.h>
#include <stdio.h>
#include <vector>

#include "../shared/erfcmatching.h"
#include "../shared/cpacmatching.h"
#include "../shared/thrsmatching.h"

#include "ITKIOFactoryRegistration/itkImageIOFactoryRegisterManager.h"
#include "ITKIOFactoryRegistration/itkTransformIOFactoryRegisterManager.h"
#include <itkImageIOFactory.h>
#include <itkImageFileReader.h>
#include <itkContinuousIndex.h>
#include <itkLinearInterpolateImageFunction.h>
#include <itkBinaryThresholdImageFilter.h>
#include <itkBinaryFillholeImageFilter.h>

#include "../shared/vesseltreefrombinary.h"
#include <dlib/matrix.h>
typedef dlib::matrix<double, 2, 2> Covarm;

//https://gist.github.com/dennisfrancis/444dd3e918f61f898f9d1551eeb8cde0

const double pi = 3.141592653589793;
const double p2 = 1.414213562373095;

const unsigned int num_of_dirs = 252;
const unsigned int num_of_samples = 100;
const double dist_of_samples = 0.2;
const unsigned char threshold_value = 159;

ImageType::Pointer thresholdFilter(ImageType::Pointer input_image, PixelType min, PixelType max)
{
    typedef itk::BinaryThresholdImageFilter< ImageType, ImageType > F;
    typedef F::Pointer P;
    P filter = F::New();
    filter->SetInput(input_image);
    filter->SetLowerThreshold(min);
    filter->SetUpperThreshold(max);
    filter->SetInsideValue(255);
    filter->SetOutsideValue(0);
    filter->Update();
    return filter->GetOutput();
}

ImageType::Pointer fillInBlackHoles(ImageType::Pointer input_image)
{
    typedef typename itk::BinaryFillholeImageFilter < ImageType > F;
    typedef typename F::Pointer P;
    P filter = F::New();
    filter->SetInput(input_image);
    filter->SetForegroundValue(255);
    filter->Update();
    return filter->GetOutput();
}

struct Vector2
{
    double x;
    double y;
};

struct Vector3
{
    double x;
    double y;
    double z;
};

void findOrthogonalDirections(Vector3& v, std::vector <Vector3>* orthogonals)
{
   Vector3 s;
   Vector3 c;
   if(fabs(v.x) < fabs(v.y) && fabs(v.x) < fabs(v.z))
   {
       s.x = 0.0;
       s.y = v.z;
       s.z = -v.y;
   }
   else if(fabs(v.y) < fabs(v.z) && fabs(v.y) < fabs(v.x))
   {
       s.y = 0.0;
       s.z = v.x;
       s.x = -v.z;
   }
   else
   {
       s.z = 0.0;
       s.x = v.y;
       s.y = -v.x;
   }
   c.x = v.z*s.y - v.y*s.z;
   c.y = v.x*s.z - v.z*s.x;
   c.z = v.y*s.x - v.x*s.y;

   double dc = sqrt(c.x*c.x + c.y*c.y + c.z*c.z);
   double ds = sqrt(s.x*s.x + s.y*s.y + s.z*s.z);
   c.x /= dc;
   c.y /= dc;
   c.z /= dc;
   s.x /= ds;
   s.y /= ds;
   s.z /= ds;

    unsigned int imax = orthogonals->size();
    for(unsigned int i = 0; i < imax; i++)
    {
        double a = 2.0*pi*i/imax;
        double ca = cos(a);
        double sa = sin(a);
        (*orthogonals)[i].x = c.x*ca + s.x*sa;
        (*orthogonals)[i].y = c.y*ca + s.y*sa;
        (*orthogonals)[i].z = c.z*ca + s.z*sa;
    }
}

std::vector <Vector2> findVectorsFromPartialEstimates(std::vector<double>& estimates)
{
    std::vector <Vector2> vectors;
    unsigned int imax = estimates.size();
    vectors.resize(imax);
    for(unsigned int i = 0; i < imax; i++)
    {
        double a = 2.0*pi*i/imax;
        vectors[i].x = cos(a)*estimates[i];
        vectors[i].y = sin(a)*estimates[i];
    }
    return vectors;
}

double estimateRadiusByPCA(std::vector <Vector2>& vectors)
{
    double varx = 0.0;
    double vary = 0.0;
    double varxy = 0.0;

    unsigned int imax = vectors.size();
    for(unsigned int i = 0; i < imax; i++)
    {
        varx += vectors[i].x*vectors[i].x;
        vary += vectors[i].y*vectors[i].y;
        varxy += vectors[i].x*vectors[i].y;
    }
    Covarm covarm;
    covarm(0, 0) = varx/imax;
    covarm(1, 1) = vary/imax;
    covarm(0, 1) = covarm(1, 0) = varxy/imax;

    dlib::eigenvalue_decomposition<Covarm> eigdec(covarm);
    auto eigvals = eigdec.get_real_eigenvalues();
    double r = sqrt(fabs(eigvals(0)) + fabs(eigvals(1)));
    return r;
}

int main(int argc, char *argv[])
{
    if(argc < 2)
        return 1;

    double ground_truth_radius = 0;
    if(argc > 2)
        ground_truth_radius = atof(argv[2]);

    typedef itk::ImageFileReader<ImageType> ReaderType;
    ReaderType::Pointer reader = ReaderType::New();
    reader->SetFileName(argv[1]);
    try
    {
        reader->Update();
    }
    catch (itk::ExceptionObject &ex)
    {
        std::cout << ex << std::endl;
        return false;
    }
    ImageType::Pointer image = reader->GetOutput();
    Tree tree;
    if(argc > 3)
        tree = loadTree(argv[3]);

    if(tree.branches.size() == 0 || tree.nodes.size() == 0)
    {
        ImageType::Pointer thresholded_image = thresholdFilter(image, threshold_value, 255);
        ImageType::Pointer binary_image = fillInBlackHoles(thresholded_image);
        tree = treeFromBinaryImage(binary_image);
        if(argc > 3)
            saveTree(argv[3], tree);
    }

    itk::ContinuousIndex<double, 3> pixel;
    itk::LinearInterpolateImageFunction<ImageType, double>::Pointer interpolator =
      itk::LinearInterpolateImageFunction<ImageType, double>::New();
    interpolator->SetInputImage(image);
    ImageType::SizeType imagesize = image->GetLargestPossibleRegion().GetSize();
    double erfc_average = 0.0;
    double erfc_squared = 0.0;
    double cpac_average = 0.0;
    double cpac_squared = 0.0;
    double thrs_average = 0.0;
    double thrs_squared = 0.0;
    unsigned int number_of_nodes = 0;
    unsigned int number_of_branches = 0;
    unsigned int bmax = tree.branches.size();
    for(unsigned int b = 0; b < bmax; b++)
    {
        unsigned int nmax = tree.branches[b].node_indices.size();
        if(nmax < 3)
            continue;
        bool countthis = false;
        for(unsigned int n = 1; n < nmax-1; n++)
        {
            Node* nm = &(tree.nodes[tree.branches[b].node_indices[n-1]]);
            Node* np = &(tree.nodes[tree.branches[b].node_indices[n+1]]);
            Node* nn = &(tree.nodes[tree.branches[b].node_indices[n]]);
            double x = nn->x - (double)imagesize[0]/2.0;
            double y = nn->y - (double)imagesize[1]/2.0;
            double z = nn->z - (double)imagesize[2]/2.0;
            if(x*x+y*y+z*z > 25*25)
                continue;

            Vector3 v;
            v.x = nm->x - np->x;
            v.y = nm->y - np->y;
            v.z = nm->z - np->z;
            std::vector <Vector3> dirs;
            dirs.resize(num_of_dirs);
            findOrthogonalDirections(v, &dirs);
            std::vector<std::vector<double> > data;
            for(unsigned int d = 0; d < num_of_dirs; d++)
            {
                std::vector<double> line;
                line.resize(num_of_samples);
                for(unsigned int i = 0; i < num_of_samples; i++)
                {
                    pixel[0] = dirs[d].x*dist_of_samples*i + nn->x;
                    pixel[1] = dirs[d].y*dist_of_samples*i + nn->y;
                    pixel[2] = dirs[d].z*dist_of_samples*i + nn->z;
                    if(pixel[0] < 0) pixel[0] = 0;
                    if(pixel[0] > imagesize[0]-1) pixel[0] = imagesize[0]-1;
                    if(pixel[1] < 0) pixel[1] = 0;
                    if(pixel[1] > imagesize[1]-1) pixel[1] = imagesize[1]-1;
                    if(pixel[2] < 0) pixel[2] = 0;
                    if(pixel[2] > imagesize[2]-1) pixel[2] = imagesize[2]-1;
                    line[i] = interpolator->EvaluateAtContinuousIndex(pixel);
                }
                data.push_back(line);
            }

            countthis = true;

            image_threshold = threshold_value;
            std::vector<double> pthrs = thrsMatching(data);
            std::vector<Vector2> vthrs = findVectorsFromPartialEstimates(pthrs);
            double rthrs = dist_of_samples*estimateRadiusByPCA(vthrs);
            thrs_average += rthrs;
            thrs_squared += rthrs*rthrs;

            snake_image_threshold = threshold_value;
            std::vector<double> pcpac = cpacMatching(data);
            std::vector<Vector2> vcpac = findVectorsFromPartialEstimates(pcpac);
            double rcpac = dist_of_samples*estimateRadiusByPCA(vcpac);
            cpac_average += rcpac;
            cpac_squared += rcpac*rcpac;

            std::vector<double> perfc = erfcMatching(data);
            std::vector<Vector2> verfc = findVectorsFromPartialEstimates(perfc);
            double rerfc = dist_of_samples*estimateRadiusByPCA(verfc);
            erfc_average += rerfc;
            erfc_squared += rerfc*rerfc;

            number_of_nodes++;
        }
        if(countthis)
            number_of_branches++;
    }
    thrs_average /= number_of_nodes;
    thrs_squared /= number_of_nodes;
    thrs_squared = sqrt(thrs_squared - thrs_average*thrs_average);

    cpac_average /= number_of_nodes;
    cpac_squared /= number_of_nodes;
    cpac_squared = sqrt(cpac_squared - cpac_average*cpac_average);

    erfc_average /= number_of_nodes;
    erfc_squared /= number_of_nodes;
    erfc_squared = sqrt(erfc_squared - erfc_average*erfc_average);

    printf("%i, %i, %f, %f, %f, %f, %f, %f, %f\n",
           number_of_branches, number_of_nodes,
           ground_truth_radius,
           thrs_average, thrs_squared,
           cpac_average, cpac_squared,
           erfc_average, erfc_squared);
    return 0;
}



//Test
//int main(int argc, char *argv[])
//{
//    std::vector<std::vector<double> > data;
//    for(unsigned int i = 0; i < 11; i++)
//    {
//        std::vector<double> line;
//        for(unsigned int i = 0; i < 100; i++)
//        {
//            if(i < 67)
//                line.push_back(192.0);
//            else
//                line.push_back(63.0);
//        }
//        data.push_back(line);
//    }
//    std::vector<double> erfc = erfcMatching(data);
//    std::vector<double> cpac = cpacMatching(data);
//    std::vector<double> thrs = thrsMatching(data);
//    printf("ERFC ");
//    for(unsigned int i = 0; i < erfc.size(); i++)
//    {
//        printf("%f, ", (float) erfc[i]);
//    }
//    printf("\n");
//    printf("CPAC ");
//    for(unsigned int i = 0; i < cpac.size(); i++)
//    {
//        printf("%f, ", (float) cpac[i]);
//    }
//    printf("\n");
//    printf("THRS ");
//    for(unsigned int i = 0; i < thrs.size(); i++)
//    {
//        printf("%f, ", (float) thrs[i]);
//    }
//    printf("\n");
//    return 0;
//}
