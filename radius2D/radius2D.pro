TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

DESTDIR         = ../build
TARGET = radius2D

SOURCES += \
    radius2D.cpp \
    ../shared/erfcmatching.cpp \
    ../shared/cpacmatching.cpp \
    ../shared/thrsmatching.cpp \
    ../shared/vesseltree.cpp \
    ../shared/vesseltreefrombinary.cpp

INCLUDEPATH += /home/piotr/Program/ThirdParty/usr/include/
LIBS += -lblas
LIBS += -llapack
LIBS += /home/piotr/Program/ThirdParty/usr/lib/libdlib.a

HEADERS += \
    ../shared/erfcmatching.h \
    ../shared/cpacmatching.h \
    ../shared/thrsmatching.h \
    ../shared/vesseltreefrombinary.h

INCLUDEPATH += ../../vesselknife/
include(../../vesselknife/Pri/itk.pri)
