/*
 * Quasi-randomly finds directions in 3D space
 *
 * Copyright 2020 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include "../shared/sphear.h"
#include "../shared/vesseltree.h"

const unsigned int divider = 99;
const unsigned int samples = 11;

int main(int argc, char *argv[])
{
    Sphear sphear(divider);
    unsigned int vn = sphear.verticesNumber();
    srand(1234321);
    float vectors[samples][3];

    for(unsigned int k = 0; k < samples; k++)
    {
        double vector[3];
        unsigned int kk = 0;
        do
        {
            unsigned int v = rand()%vn;
            sphear.vertex(v, vector);
            for(; kk < k; kk++)
            {
                if((float)vector[0] == vectors[kk][0] && (float)vector[1] == vectors[kk][1] && (float)vector[2] == vectors[kk][2])
                    break;
                if((float)vector[0] == -vectors[kk][0] && (float)vector[1] == -vectors[kk][1] && (float)vector[2] == -vectors[kk][2])
                    break;
            }
        }
        while(kk < k);
        vectors[k][0] = (float)vector[0];
        vectors[k][1] = (float)vector[1];
        vectors[k][2] = (float)vector[2];
    }

    if(argc < 3)
    {
//        printf("x\ty\tz\tvn=%i rm=%i\n", vn, RAND_MAX);
//        for(unsigned int k = 0; k < samples; k++)
//        {
//            printf("%f\t%f\t%f\n", (float)vectors[k][0], (float)vectors[k][1], (float)vectors[k][2]);
//            fflush(stdout);
//        }
        for(unsigned int k = 0; k < samples; k++)
        {
            char filename[256];
            sprintf(filename, "mt%.4itree.txt", k);

            Tree tree;
            tree.branches.resize(1);
            for(int i = -25; i <= 25; i++)
            {
                Node node;
                node.radius = 0.0;
                node.connections = (i == 25 || i == -25) ? 1 : 2;
                node.x = vectors[k][0]*i +49.5;
                node.y = vectors[k][1]*i +49.5;
                node.z = vectors[k][2]*i +49.5;
                tree.nodes.push_back(node);
                tree.branches[0].node_indices.push_back(i + 25);
            }
            saveTree(filename, tree);
        }
    }
    if(argc >= 3)
    {
        float radius = atof(argv[1]);
        float noise = atof(argv[2]);

        for(unsigned int k = 0; k < samples; k++)
        {
            char filename[256];
            sprintf(filename, "mt%.4iconf.txt", k);
            FILE *fp = fopen(filename, "w");
            if(fp != NULL)
            {
                fprintf(fp, "# This is a tube model configuration file.\n");
                fprintf(fp, "# It is an input for makeTubes program.\n");
                fprintf(fp, "BackgroundGreyLevel 64\n");
                fprintf(fp, "ImageSize 100 100 100\n");
                fprintf(fp, "PartialVolumeDividers 4 4 4\n");
                fprintf(fp, "VoxelSpacing 1 1 1\n");
                fprintf(fp, "GaussianNoise %f\n",
                            noise
                        );
                fprintf(fp, "Tube %f %f %f %f %f %f %f 192\n",
                            radius,
                            (float)vectors[k][0]*100.0, (float)vectors[k][1]*100.0, (float)vectors[k][2]*100.0,
                            -(float)vectors[k][0]*100.0, -(float)vectors[k][1]*100.0, -(float)vectors[k][2]*100.0
                        );
//                fprintf(fp, "Elipsoid %f %f %f %f %f %f 0 0 0 192\n",
//                        radius, radius, radius,
//                        (float)vectors[k][0]*40.0, (float)vectors[k][1]*40.0, (float)vectors[k][2]*40.0
//                        );
//                fprintf(fp, "Elipsoid %f %f %f %f %f %f 0 0 0 192\n",
//                        radius, radius, radius,
//                        -(float)vectors[k][0]*40.0, -(float)vectors[k][1]*40.0, -(float)vectors[k][2]*40.0
//                        );
                fprintf(fp, "Elipsoid 150 150 150 -150 0 0 0 0 0 128\n");
                fclose(fp);
            }
        }
    }

    return 0;
}
