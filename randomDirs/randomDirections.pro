TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

DESTDIR         = ../build
TARGET = randomDirections

SOURCES += \
    randomDirections.cpp \
    ../shared/sphear.cpp \
    ../shared/vesseltree.cpp

HEADERS += \
    ../shared/sphear.h \
    ../shared/vesseltree.h
